import {expect} from 'chai';
import {
    add
} from './kata.js';

describe('Kata', () => {
    it('should add', () => {
        expect(add(1, 2)).to.equal(3);
    });
});
