module.exports = {
    env: {
        browser: true,
        es6: true,
        mocha: true
    },
    extends: 'eslint:recommended',
    parserOptions: {
        sourceType: 'module'
    },
    rules: {
        indent: ['error', 2],
        'linebreak-style': ['error', 'unix'],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        eqeqeq: ['error', 'always'],
        'arrow-parens': ['error', 'as-needed'],
        'arrow-spacing': 'error',
        'arrow-body-style': ['error', 'as-needed'],
        'prefer-arrow-callback': 'error',
        'prefer-const': 'error',
        'prefer-spread': 'error',
        'no-var': 'error',
    }
};
