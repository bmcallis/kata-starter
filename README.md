# Kata Starter

## Getting Started

Clone the repo with minimal history.

`git clone --depth=1 --branch=master git://someserver/somerepo dirformynewrepo`

Then remove the `.git` directory.

`rm -rf !$/.git`

If you desire start your own git history for it

`git init`

Install all of the needed node modules

`npm install` or `yarn install`

Now you can start the project to watch for file changes and rerun the tests anytime there is a change.

`npm start`

### Debugging

Running `npm start` will start mocha in watch mode with the inspector flag. This allows you to hit a URL in chrome to debug the app. To find the url you can go to `about:inspect` and look under `Remote Target #localhost`. Then clicking `inspect` will allow you to debug, just save a file after setting a break point to get the tests to run again.
